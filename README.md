# Tutorium PM2 SS 23

*Die Lösungen zu den Aufgaben findet ihr
im [`lösungen`](./../-/tree/lösungen)-Branch*


## Themen der Termine

| Termin | Thema                                                               | Datum      | Links                                                                                                                                                                                                                                                                                                             |
|--------|---------------------------------------------------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | Abstrakte Klassen, Interfaces und Regex | 30.03.23 | **[Folien](./../../../-/raw/master/Termin01/folien.pdf)** **[Aufgaben](./../../../-/blob/master/Termin01/aufgaben.md)** **[Dateien](./../../../-/tree/master/Termin01/dateien)** |
