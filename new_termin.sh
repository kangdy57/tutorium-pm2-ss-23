#!/bin/bash
# More safety, by turning some bugs into errors.
# Without `errexit` you don’t need ! and can replace
# ${PIPESTATUS[0]} with a simple $?, but I prefer safety.
set -o errexit -o pipefail -o noclobber -o nounset

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

# option --output/-o requires 1 argument
LONGOPTS=slides,tasks,files,append-readme,date:,help
OPTIONS=stfrd:h

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

s=n t=n f=n r=n d="??.??.??"
# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -s|--slides)
            s=y
            shift
            ;;
        -t|--tasks)
            t=y
            shift
            ;;
        -f|--files)
            f=y
            shift
            ;;
        -r|--append_readme)
            r=y
            shift
            ;;
	-d|--date)
	    d="$2"
	    shift 2
	    ;;
        -h|--help)
	    echo "Aufruf: $0 [Optionen] Thema des Termins"
	    echo ""
	    echo "Optionen"
	    echo "  -s, --slides		Es gibt Folien in diesem Termin"
	    echo "  -t, --tasks		Es gibt Aufgaben in diesem Termin"
	    echo "			(eine aufgaben.md-Datei wird automatisch erstellt)"
	    echo "  -f, --files		Es gibt Dateien in diesem Termin"
	    echo "			(ein \"dateien\"-Ordner wird automatisch erstellt)"
	    echo "  -d, --date=<Datum>	Datum des Termins"
	    echo "  -r, --append-readme 	Der neue Termin wird an die README.md-Tabelle angehängt"
	    echo "  -h, --help		Diese Hilfe"

	    exit 0
	    ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# handle non-option arguments
if [[ $# -lt 1 ]]; then
    echo "$0: Es muss ein Thema angegeben werden."
    exit 4
fi


new_number=$(find . -type d -name 'Termin*' | sort | tail -1 | cut -b 9- | awk '{print $0+1}')
if [ "$new_number" == "" ]; then
	new_number="1"
fi

new_number_padded=$(printf "%02d" "$new_number")
dir="./Termin$new_number_padded"

echo "Erzeuge Termin $new_number - $@ - am $d"
echo "=================="
echo "Folien: $s"
echo "Aufgaben: $t"
echo "Dateien: $f"
echo ""
echo "Termin an README.md anhängen: $r"

mkdir "$dir"

slides_text="*keine Folien*"
if [ "$s" == "y" ]; then
	slides_text="**[Folien](./../../../-/raw/master/Termin$new_number_padded/folien.pdf)**"
fi

tasks_text="*keine Aufgaben*"
if [ "$t" == "y" ]; then
	tasks_text="**[Aufgaben](./../../../-/blob/master/Termin$new_number_padded/aufgaben.md)**"
	echo -e "# $@ - Termin $new_number\n\n## Theorieaufgaben:\n\n\n## Praxisaufgaben:\n" > "$dir/aufgaben.md"
fi

files_text="*keine Dateien*"
if [ $f == "y" ]; then
	files_text="**[Dateien](./../../../-/tree/master/Termin$new_number_padded/dateien)**"
	mkdir "$dir/dateien"
	touch "$dir/dateien/.gitkeep"
fi

if [ $r == "y" ]; then
	echo "| $new_number | $@ | $d | $slides_text $tasks_text $files_text |" >> README.md
fi
