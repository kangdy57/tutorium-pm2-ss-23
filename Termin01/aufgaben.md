# Abstrakte Klassen, Interfaces und Regex - Termin 1

## Theorieaufgaben:

1. Wann sollte eine Klasse mit einer abstrakten Klasse ersetzt werden?
2. Welche Typen von Methoden gibt es und wofür braucht man diese jeweils?
3. Muss der Programmierer überprüfen, ob eine abstrakte Methode fälschlicherweise nicht konkret implementiert wurde?
4. Kann eine Klasse am Ende der Vererbungshierarchie eine abstrakte Klasse sein?
5. Wie viele Typen kann ein Objekt in Java besitzen? Begründe deine Antwort.
6. Welche Probleme treten auf, wenn man ein vorhandenes Interface nachträglich um eine Methode ergänzt? Wie kann man dieses Problem verhindern/umgehen?
7. Erkläre (gerne deinem Nachbarn) in eigenen Worten, inwiefern Interfaces eine Schnittstelle schaffen, die andere Objekte nutzen können.
8. Schreibe einen regulären Ausdruck, der die Zahl Pi bei mindestens 2 und maximal 5 Nachkommastellen erkennt.
9. Was ist der Unterschied zwischen einem greedy und einem reluctant Quantifier? Nenne 2 Beispiele, bei denen ein reluctant Quantifier benötigt wird.
10. Schreibe einen regulären Ausdruck für ein (unsicheres) Passwort, das mit einem Großbuchstaben beginnen, einen Punkt enthalten und mit einem Ausrufezeichen sowie 1-10 Ziffern enden soll (Leerzeichen dürfen an keiner Stelle vorkommen). 

## Praxisaufgaben:

### Abstrakte Klassen & Interfaces
Modelliere das folgende Szenario, welches von Containern und Waren handelt:
1. Container treten immer spezialisiert auf.
2. Container werden bei der Erzeugung aufsteigend nummeriert und haben eine gewisse Anzahl Plätze für ihre Waren.
3. Ein Warencontainer beinhaltet verschiedene Waren bis hin zu einer maximalen Anzahl (es wird einfach davon ausgegangen, dass eine Ware genau einen Platz beansprucht).
4. Ein Spezialcontainer beinhaltet maximal eine Ware (weil diese besonders groß ist oder so).
5. Ein Container muss sein Gewicht berechnen, sich mit einer zwingenden Platzprüfung beladen und entladen können.
6. Die Ware kann ein beliebiges Etwas sein.
7. Eine Ware kann mindestens über ihr eigenes Gewicht und ihre ID informieren.

### Reguläre Ausdrücke
Schreibe einen Algorithmus, der die Beispiel-HTML aus diesem Ordner in eine Markdown-Datei mit einem gleichwertigen Aussehen parsed. 
